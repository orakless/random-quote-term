import random
import json
import os
import re

HOME = os.getenv("HOME")

with open(f'{HOME}/.scripts/shell/catchphrases.json') as file:
    QUOTES = json.load(file)

rows, columns = os.popen('stty size', 'r').read().split()
max_size = int(int(columns) * 0.6)
padding = int((int(columns) - max_size) / 2)

quote_nb = random.randrange(len(QUOTES['catchphrases']))

raw_name = QUOTES['catchphrases'][quote_nb]['name']
raw_author = QUOTES['catchphrases'][quote_nb]['character']
raw_source = QUOTES['catchphrases'][quote_nb]['serie']

raw_quote = raw_name.split("\n")
raw_quote_source = [f"- {raw_author}, from {raw_source}"]
temporary_line = ""

quote = []
quote_source = []


for line in raw_quote:
    if len(line) > max_size:
        line = line.split(" ")
        for word in line:
            if len(temporary_line) + len(word) + 1 <= max_size and line.index(word) != len(line):
                temporary_line = f"{temporary_line}{word} "
            else:
                quote.append(temporary_line[:-1])
                temporary_line = f"{word} "
        if len(temporary_line) != 0:
            quote.append(temporary_line[:-1])
            temporary_line = ""
    else:
        quote.append(line)

for lines in raw_quote_source:
    if len(lines) > max_size:
        lines = lines.split(" ")
        for word in lines:
            if len(temporary_line) + len(word) + 1 <= max_size and lines.index(word) != len(lines):
                temporary_line = f"{temporary_line}{word} "
            else:
                quote_source.append(temporary_line[:-1])
                temporary_line = f"{word} "
        if len(temporary_line) != 0:
            quote_source.append(temporary_line[:-1])
    else:
        quote_source.append(lines)


def printall(padding, max_size, quote):
    padding = padding-1
    print(f"\033[1m{'*'*int(columns)}\033[0m")
    print(f"\033[1m*{' '*(int(columns)-2)}*")
    for line in quote:
        padding_right = int(columns) - (padding + len(line)) - 2
        print(f"\033[1m*\033[0m{' '*padding}", end="")
        print(f"\033[1m{line}\033[0m", end="")
        print(f"{' '*padding_right}\033[1m*\033[0m", end="\n")
    for lines in quote_source:
        padding = padding+(max_size-len(max(quote_source, key=len)))
        padding_right = int(columns) - (padding + len(lines)) - 2
        print(f"\033[1m*\033[0m{' '*padding}", end="")
        print(lines, end="")
        print(f"{' '*padding_right}\033[1m*\033[0m", end="\n")
    print(f"\033[1m*{' '*(int(columns)-2)}*")
    print(f"\033[1m{'*'*int(columns)}\033[0m")
    print("                  \033[1m  ******\033[0m\n"
          "           ^__^   \033[1m ****\033[0m\n"
          "    _______(oo)   \033[1m**\033[0m\n"
          r"/\/(      \(__)  "
          "\n"
          r"   | w----||"
          "\n"
          r"   ||     ||")


if len(max(quote, key=len)) > len(max(quote_source, key=len)):
    max_size = len(max(quote, key=len))
else:
    max_size = len(max(quote_source, key=len))
padding = int((int(columns)-max_size)/2)
printall(padding, max_size, quote)
